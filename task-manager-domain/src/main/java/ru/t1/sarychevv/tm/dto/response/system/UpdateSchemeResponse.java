package ru.t1.sarychevv.tm.dto.response.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.response.AbstractResultResponse;

public final class UpdateSchemeResponse extends AbstractResultResponse {

    public UpdateSchemeResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
