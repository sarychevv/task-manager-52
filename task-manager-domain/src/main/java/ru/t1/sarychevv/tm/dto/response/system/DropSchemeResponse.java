package ru.t1.sarychevv.tm.dto.response.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.response.AbstractResultResponse;

public final class DropSchemeResponse extends AbstractResultResponse {

    public DropSchemeResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
