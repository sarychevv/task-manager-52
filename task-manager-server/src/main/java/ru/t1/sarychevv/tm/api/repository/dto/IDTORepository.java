package ru.t1.sarychevv.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.AbstractModelDTO;

import java.util.Comparator;
import java.util.List;

public interface IDTORepository<M extends AbstractModelDTO> {

    @NotNull
    M add(@NotNull M model) throws Exception;

    void removeAll() throws Exception;

    boolean existsById(@NotNull String id) throws Exception;

    @Nullable
    List<M> findAll() throws Exception;

    @Nullable
    List<M> findAll(@Nullable Comparator comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String id) throws Exception;

    int getSize() throws Exception;

    @NotNull
    M removeOne(@NotNull M model) throws Exception;

    @NotNull
    M update(@NotNull M model) throws Exception;


}
