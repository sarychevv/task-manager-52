package ru.t1.sarychevv.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.dto.IDTORepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.dto.IDTOService;
import ru.t1.sarychevv.tm.dto.model.AbstractModelDTO;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IDTOService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract IDTORepository<M> getRepository(@NotNull final EntityManager entityManager);

    @NotNull
    @Override
    public M add(@NotNull final M model) throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) return false;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            return repository.existsById(id);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            return repository.findAll(comparator);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) throws Exception {
        if (sort == null) return findAll();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            return repository.findAll(sort.getComparator());
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            return repository.findOneById(id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public int getSize() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            return repository.getSize();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public M removeOne(@Nullable final M model) throws Exception {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    public M removeOneById(@Nullable final String id) throws Exception {
        @Nullable M result = findOneById(id);
        return removeOne(result);
    }

    @NotNull
    @Override
    public M update(@Nullable final M model) throws Exception {
        if (model == null) throw new ModelNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IDTORepository<M> repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return model;
    }

}

